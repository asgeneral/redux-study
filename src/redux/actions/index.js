import {
    HISTORY_ACTIONS,
    actionAddTodo,
    actionCompleteTodo,
    actionDeleteTodo,
    actionPutTodo,
    actionEditTodo,
    actionExpandTodo,
    actionInitialSettingTodo,
    actionToggleTodo,
    actionSetDescriptionTodo,
    actionAddHistoryRecord, TODO_ACTIONS,
} from './actionCreators'


function setCompleteTodo(id) {
    return (dispatch) => {
        dispatch(actionCompleteTodo(id));
        dispatch(actionAddHistoryRecord({
            type: TODO_ACTIONS.COMPLETE_TODO,
            id,
        }))
    };
}

function addNewTodo() {
    return (dispatch) => {
        dispatch(actionAddTodo());
    }
}

function expandTodo(data) {
    return (dispatch) => {
        const { id } = data;
        dispatch(actionExpandTodo(id));
    }
}

function deleteTodo(id) {
    return (dispatch) => {
        dispatch(actionDeleteTodo(id));
        dispatch(actionAddHistoryRecord({
           type: HISTORY_ACTIONS.DELETE_TODO,
            id,
        }))
    }
}

function setDescriptionTodo(id, value) {
    return (dispatch) => {
        dispatch(actionSetDescriptionTodo({
            id,
            value,
        }))
    }
}

function setInitialSetting() {
    return (dispatch) => {
        dispatch(actionInitialSettingTodo);
    }
}

function toggleTodo(id) {
    return (dispatch) => {
        dispatch(actionToggleTodo(id))
    }
}

function editTodo(id) {
    return (dispatch) => {
        dispatch(actionEditTodo(id));
        dispatch(actionAddHistoryRecord({
            type: TODO_ACTIONS.EDIT_TODO,
            id,
        }))
    }
}

function getTodoList() {
    return async (dispatch) => {
        const response = await fetch('/fakeApi/todoList.json');
        const todoList = await response.json();

        dispatch(actionPutTodo(todoList));
        dispatch(actionInitialSettingTodo());
        dispatch(actionAddHistoryRecord({
            type: HISTORY_ACTIONS.GET_TODO_LIST,
        }))
    }
}


export {
    getTodoList,
    toggleTodo,
    editTodo,
    setCompleteTodo,
    setInitialSetting,
    setDescriptionTodo,
    deleteTodo,
    addNewTodo,
    expandTodo,
}