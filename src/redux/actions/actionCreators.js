const TODO_ACTIONS = {
    ADD_TODO: 'ADD_TODO',
    GET_TODO: 'GET_TODO',
    COMPLETE_TODO: 'COMPLETE_TODO',
    DELETE_TODO: 'DELETE_TODO',
    EXPAND_TODO: 'EXPAND_TODO',
    COLLAPSE_TODO: 'COLLAPSE_TODO',
    TOGGLE_TODO: 'TOGGLE_TODO',
    SET_INITIAL_SETTING: 'SET_INITIAL_SETTING',
    EDIT_TODO: 'EDIT_TODO',
    SET_DESCRIPTION: 'SET_DESCRIPTION',
};

const CORE_ACTIONS = {
    ADD_HISTORY_RECORD: 'ADD_HISTORY_RECORD',
}

const HISTORY_ACTIONS = {
    DELETE_TODO: 'DELETE_TODO',
    GET_TODO_LIST: 'GET_TODO_LIST',
}

const actionAddTodo = () => ({
    type: TODO_ACTIONS.ADD_TODO,
})

const actionEditTodo = (id) => ({
    type: TODO_ACTIONS.EDIT_TODO,
    data: id
})

const actionInitialSettingTodo = () => ({
    type: TODO_ACTIONS.SET_INITIAL_SETTING,
})

const actionToggleTodo = (id) => ({
    type: TODO_ACTIONS.TOGGLE_TODO,
    data: id,
})

const actionDeleteTodo = (id) => ({
    type: TODO_ACTIONS.DELETE_TODO,
    data: id,
})

const actionPutTodo = (data) => ({
    type: TODO_ACTIONS.GET_TODO,
    data,
})

const actionCompleteTodo = (id) => ({
    type: TODO_ACTIONS.COMPLETE_TODO,
    data: id
})

const actionSetDescriptionTodo = (data) => ({
    type: TODO_ACTIONS.SET_DESCRIPTION,
    data,
})

const actionExpandTodo = (data) => ({
    type: TODO_ACTIONS.EXPAND_TODO,
    data,
})

const actionAddHistoryRecord = (data) => ({
    type: CORE_ACTIONS.ADD_HISTORY_RECORD,
    data,
})
export {
    TODO_ACTIONS,
    CORE_ACTIONS,
    HISTORY_ACTIONS,
    actionAddTodo,
    actionCompleteTodo,
    actionDeleteTodo,
    actionPutTodo,
    actionEditTodo,
    actionInitialSettingTodo,
    actionToggleTodo,
    actionSetDescriptionTodo,
    actionAddHistoryRecord,
    actionExpandTodo,
}