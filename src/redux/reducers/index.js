import {
    TODO_ACTIONS,
    CORE_ACTIONS,
} from "../actions/actionCreators";

import { combineReducers } from 'redux'

const todoListInitialState = {
    todoList: []
}

const todoList = (state = todoListInitialState,{ type, data }) => {
    switch (type) {
        case TODO_ACTIONS.GET_TODO: {
            return {
                ...state,
                todoList: data.filter(todo => todo.id < 15 ),
            }
        }
        case TODO_ACTIONS.DELETE_TODO: {
            return {
                ...state,
                todoList: state.todoList.filter(todo => todo.id !== data),
            }
        }
        case TODO_ACTIONS.EXPAND_TODO: {
            return {
                ...state,
                todoList: state.todoList.map(todo => todo.id !== data ? todo : {
                    ...todo,
                    collapse: false,
                } )
            }
        }
        case TODO_ACTIONS.ADD_TODO: {
            const { todoList } = state;
            const newTodoId = todoList.reduce((maxId, currentTodo) => (currentTodo.id > maxId) ? currentTodo.id : maxId, todoList[0].id);
            const newTodo = {
                id: newTodoId + 1,
                completed: false,
                collapse: true,
                userId: 1,
                title: "It is new task, please edit it as you like",
            }
            const newTodoList = [...todoList]
            newTodoList.splice(0, 0, newTodo);
            return {
                ...state,
                todoList: newTodoList,
            }
        }
        case TODO_ACTIONS.COMPLETE_TODO: {
            return {
                ...state,
                todoList: state.todoList.map(todo => {
                    return (todo.id !== data) ? todo : {
                        ...todo,
                        completed: true,
                    }
                })
            }
        }
        case TODO_ACTIONS.TOGGLE_TODO: {
            return {
                ...state,
                todoList: state.todoList.map(todo => {
                    return (todo.id !== data) ? todo : {
                        ...todo,
                        collapse: !todo.collapse,
                    }
                })
            }
        }
        case TODO_ACTIONS.SET_INITIAL_SETTING: {
            return {
                ...state,
                todoList: state.todoList.map(todo => ({
                    ...todo,
                    collapse: true,
                    edit: false,
                }))
            }
        }
        case TODO_ACTIONS.EDIT_TODO: {
            return {
                ...state,
                todoList: state.todoList.map(todo => {
                    return (todo.id !== data) ? todo : {
                        ...todo,
                        edit: !todo.edit,
                    }
                })
            }
        }
        case TODO_ACTIONS.SET_DESCRIPTION: {
            return {
                ...state,
                todoList: state.todoList.map(todo => {
                    return (todo.id !== data.id) ? todo : {
                        ...todo,
                        title: data.value,
                    }
                })
            }
        }
        case TODO_ACTIONS.COLLAPSE_TODO: {
            return {
                ...state,
                todoList: state.todoList.map(todo => {
                    return (todo.id !== data) ? todo : {
                        ...todo,
                        collapse: false
                    }
                })
            }
        }
        default: {
            return {
                ...state,
            }
        }
    }
}

const coreInitialState = {
    history: [],
}

const core = (state = coreInitialState, {type, data}) => {
    switch (type) {
        case CORE_ACTIONS.ADD_HISTORY_RECORD:
            const date = new Date();
            const recordData = {
                ...data,
                date,
            }
            return {
                ...state,
                history: [...state.history, recordData]
            }
        default:
            return {
                ...state
            }
    }
}

const reducer = combineReducers({
    todo: todoList,
    core,
})

export { reducer }