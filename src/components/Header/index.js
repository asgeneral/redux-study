import React from 'react'
import { Link } from 'react-router-dom'
import './style.less'

class Header extends React.Component {
    constructor() {
        super();
    }

    render() {
        return <div className='header'>
            <ul className='header__navList'>
                <li><Link to='/'><button className='navList__element'>Home</button></Link></li>
                <li><Link to='/my-tasks'><button className='navList__element'>My tasks</button></Link></li>
                <li><Link to='/filter'><button className='navList__element'>Filter</button></Link></li>
            </ul>
        </div>
    }
}

export { Header }