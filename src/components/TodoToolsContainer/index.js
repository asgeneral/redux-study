import React from 'react';
import { TodoTools } from "../TodoTools";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { addNewTodo, expandTodo } from "../../redux/actions";

function mapStateToProps(state) {
    return {
        todoList: state.todo.todoList,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewTodo,
        expandTodo,
    }, dispatch);
}

class TodoToolsContainerClass extends React.Component {

    async handleAddTodo() {
        const { addNewTodo, expandTodo, todoList } = this.props;
        console.log(this.props);
        await addNewTodo();
        console.log()
    }

    render() {
        return (
            <TodoTools
                handleAddTodo={() => this.handleAddTodo()}
            />
        )
    }
}

const TodoToolsContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TodoToolsContainerClass)

export { TodoToolsContainer }