import React from 'react'
import './style.less'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

function mapStateToProps(state) {
    return {
        historyList: state.core.history,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}


const History = ({historyList}) => {
    console.log(historyList);
    return (<div className='history'>
        <div className='history__topic'>History</div>
        <ul className='history__history-list'>
            {historyList.map((record, i) => (
                <li className='history-list__item' key={i}>
                    <span className='history-list__date'>{record.date.toLocaleString()}</span>: <span className='history-list__action'>{record.type}</span>
                    </li>
            ))}
        </ul>
    </div>)
}

const HistoryContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(History)

export { HistoryContainer }