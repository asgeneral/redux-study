import React from 'react'
import './style.less'

const TODO_STYLE = {
    collapseFullViewTodo: {
        height: '0',
    },
    expandFullViewTodo: {
        height: '120px',
    },
    collapseContainerTodo: {
        height: '30px',
    },
    expandContainerTodo: {
        height: '140px',
    },
}



class Todo extends React.Component{
    constructor({ title }) {
        super();
        this.titleRef = React.createRef();
        this.containerRef = React.createRef();
        this.fullViewRef = React.createRef();
        this.idRef = React.createRef();
        this.state = {
            description: title,
        }
    }

    handleOnChangeTextArea = (e) => {
        this.setState({ description: e.target.value})
    }

    localDeleteAction = () => {
        const { handleDeleteTodo } = this.props;
        const deleteTodo = () => handleDeleteTodo();
        this.idRef.current.style.opacity = 0;
        this.containerRef.current.style.cssText = "height: 0px";
        const timer = setTimeout(() => {
            deleteTodo();
        },1000);
    }

    render() {
        const { id, completed, handleComplete, collapse, handleToggle, handleEditTodo, edit, handleDeleteTodo } = this.props;
        const { description } = this.state;
        return (
            <div className='todo-container'
                 style={(collapse) ? TODO_STYLE.collapseContainerTodo : TODO_STYLE.expandContainerTodo}
                 ref={this.containerRef}
            >
                <div className='todo__id' onClick={handleToggle} ref={this.idRef}>{id}</div>
                <div className='todo__fullView'
                     style={(collapse) ? TODO_STYLE.collapseFullViewTodo : TODO_STYLE.expandFullViewTodo}
                     ref={this.fullViewRef}
                >
                    <div className='todo__fullView__description'>
                        <textarea
                            className='todo__fullView__description__title'
                            ref={this.titleRef}
                            value={description}
                            disabled={!edit}
                            onChange={this.handleOnChangeTextArea}
                        />
                        <div className='todo__fullView__description__statusBar'>
                            {(completed) ? <div className='todo__fullView__completeCheck'>⏍</div> : ''}
                        </div>
                    </div>
                    <div className='todo__fullView__description__tools'>
                        <button
                            className='fullView__complete todo__button'
                            title={(completed) ? 'in procces' : 'complete'}
                            onClick={handleComplete}>
                            {(completed) ? '⚐' : '⚑' }
                        </button>
                        <button
                            className='fullView__complete todo__button'
                            onClick={() => handleEditTodo(id, description)}
                        >✎</button>
                        <button
                            className='fullView__delete todo__button'
                            onClick={() => this.localDeleteAction()}
                        >✘</button>
                    </div>
                </div>
            </div>
        )
    }
}

export { Todo }