import React from 'react';
import './style.less'

class TodoTools extends React.Component {
    constructor() {
        super();
    }

    render() {
        const { handleAddTodo } = this.props;
        return <div className="todo-tools">
            <button
                className="todo-tools__item"
                onClick={handleAddTodo}
            >Add todo</button>
        </div>
    }
}

export { TodoTools }