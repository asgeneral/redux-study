import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {getTodoList, setCompleteTodo, expandAllTodo, collapseAllTodo, collapseTodo, expandTodo, toggleTodo, editTodo, setDescriptionTodo, deleteTodo } from "redux/actions";
import './style.less'
import {Todo} from "../Todo";
import {TodoTools} from "../TodoTools";
import {TodoToolsContainer} from "../TodoToolsContainer";

function mapStateToProps(state) {
    return {
        todoList: state.todo.todoList,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTodoList,
        setCompleteTodo,
        expandAllTodo,
        collapseAllTodo,
        collapseTodo,
        expandTodo,
        toggleTodo,
        editTodo,
        setDescriptionTodo,
        deleteTodo,
    }, dispatch)
}

class TodoList extends React.Component {

    componentDidMount() {
       if (!this.props.todoList.length) {
           this.props.getTodoList();
       }
    }

    handleCompleteTodo(id) {
        const { setCompleteTodo, todoList } = this.props;
        const [requiredTodo] = todoList.filter(todo => todo.id === id);
        if (!requiredTodo.completed) {
            setCompleteTodo(id);
        }
    }

    handleToggleTodo(id) {
        const { toggleTodo } = this.props;
        toggleTodo(id);
    }

    handleEditTodo = (id, value) => {
        const { editTodo, todoList, setDescriptionTodo } = this.props;
        editTodo(id);
        const [requiredTodo] = todoList.filter(todo => todo.id === id);
        if (requiredTodo.title !== value) {
            setDescriptionTodo(id, value);
        }
    }

    handleDeleteTodo(id) {
        const { deleteTodo } = this.props;
        deleteTodo(id);
    }d

    render() {
        const { todoList } = this.props;
        return (
            <div className='todoList-container'>
                <div className='todoList__todoTape'>
                    {todoList.map(todo => {
                        return <Todo
                            {...todo}
                            key={todo.id}
                            handleComplete={() => this.handleCompleteTodo(todo.id)}
                            handleToggle={() => this.handleToggleTodo(todo.id)}
                            handleEditTodo={this.handleEditTodo}
                            handleDeleteTodo={() => this.handleDeleteTodo(todo.id)}
                        />
                    })}
                </div>
                <TodoToolsContainer/>
            </div>
        )
    }
}

const TodoContainer = connect(mapStateToProps, mapDispatchToProps)(TodoList);

export {
    TodoContainer
}
