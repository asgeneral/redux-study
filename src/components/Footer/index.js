import React from 'react';
import './style.less';
import { HistoryContainer } from "components/History";

class  Footer extends React.Component{
    constructor() {
        super();
    }

    render() {

        return <div className='footer'>
            <HistoryContainer />
        </div>
    }
}

export { Footer }