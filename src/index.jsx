import {TodoContainer} from './components/TodoList';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { reducer } from './redux/reducers'
import thunk from 'redux-thunk';
import {applyMiddleware, compose, createStore} from 'redux';
import {
    BrowserRouter as Router,
    Route,
    Link,
} from 'react-router-dom';
import './app.less'
import {Header} from "components/Header";
import {Footer} from "components/Footer";

const root = document.getElementById('root');
const isProd = 'production' === process.env.NODE_ENV;

function getAppStore() {
    const extraArgs = {};
    const middlewares = [thunk.withExtraArgument(extraArgs)];
    const debug = !isProd;

    return createStore(
        reducer,
        {},
        compose(
            applyMiddleware(...middlewares),
            debug && window.devToolsExtension
                ? window.devToolsExtension()
                : (f) => f
        )
    );

}

const Home = () => <div>Home</div>

const Filter = () => <div>Filter</div>

ReactDOM.render(
    <Provider store={getAppStore()}>
        <Router>
            <div className='app-container'>
                <Header/>
                <Route exact path='/' component={Home}/>
                <Route path='/my-tasks' component={TodoContainer}/>
                <Route path='/filter' component={Filter}/>
                <Footer/>
            </div>
        </Router>
    </Provider>,
    root
);
